//
//  WebApiProposal.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 23/01/2024.
//

import Foundation
import DomainModel

struct WebApiProposal: Proposal {
    // MARK: Properties
    let id: Int
    let title: String
    let date: Date
    let votes: [Vote]

    // MARK: Initialisation
    static func build(
        fromRawProposal rawProposal: RawProposal,
        withPossibleCountries countries: any Collection<Country>
    ) throws -> WebApiProposal {
        let transformedVotes: [WebApiVote] = try rawProposal.votes.map {
            try WebApiVote.build(fromRawVote: $0, withPossibleCountries: countries)
        }

        return WebApiProposal(id: rawProposal.id, title: rawProposal.title, date: rawProposal.date, votes: transformedVotes)
    }
}
