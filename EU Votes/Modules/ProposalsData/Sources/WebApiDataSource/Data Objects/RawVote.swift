//
//  RawVote.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 23/01/2024.
//

import Foundation

/**
 A `Decodable` object used in `RawProposal` to parse the JSON data received from the API.

 It represents a single country's vote to a proposal.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class RawVote: Decodable {
    // MARK: Properties
    /**
     The identification number of the country who casted this vote.
     */
    let countryId: Int
    /**
     The value of the vote.

     - Note: I cannot find a definitive list of possible values here, or we could have used an enum for this property.
     */
    let value: String

    // MARK: Decodable
    private enum CodingKeys: String, CodingKey {
        case countryId = "country_id"
        case value = "vote"
    }

    /**
     Create an instance of `WebApiVote` by parsing JSON data.
     */
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // The identifier is represented as a String in the JSON.
        let countryId: String = try container.decode(String.self, forKey: .countryId)
        self.countryId = Int(countryId) ?? 0

        self.value = try container.decode(String.self, forKey: .value)
    }
}
