//
//  WebApiProposalsResponse.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 29/01/2024.
//

import Foundation

/**
This `Decodable` object is used to parse the JSON data retrieved in `WebApiDataSource`.

The data we receive from the API is not an array of proposals as we might expect, but a dictionary with arbitrary keys. As such we have to get the keys manually and loop through the dictionary to parse the proposals.

The data for each proposal is decoded as a `RawProposal` object.

- Author: Quentin BISET (qbiset@gmail.com)
*/
struct WebApiProposalsResponse: Decodable {
   // MARK: Properties
   /**
   An array of `RawProposal` objects, parsed from JSON.
   */
   let proposals: [RawProposal]

   // MARK: Decodable
   private struct DynamicKey: CodingKey {
      var intValue: Int?
      var stringValue: String

      init?(intValue: Int) {
         self.intValue = intValue
         self.stringValue = "\(intValue)"
      }

      init?(stringValue: String) {
         self.stringValue = stringValue
      }
   }

   /**
   Creates an instance of `WebApiProposalsResponse` by parsing JSON data.

   The data for each proposal is decoded as a `RawProposal` object.
   */
   init(from decoder: Decoder) throws {
      var proposalsArray: [RawProposal] = []

      let container = try decoder.container(keyedBy: DynamicKey.self)

      for key: DynamicKey in container.allKeys {
         let proposal: RawProposal = try container.decode(RawProposal.self, forKey: key)
         proposalsArray.append(proposal)
      }
      // Sorting the proposals by identifier or date should lead to the same result.
      proposalsArray.sort { (proposal1: RawProposal, proposal2: RawProposal) -> Bool in
         return proposal1.id < proposal2.id
      }

      self.proposals = proposalsArray
   }
}
