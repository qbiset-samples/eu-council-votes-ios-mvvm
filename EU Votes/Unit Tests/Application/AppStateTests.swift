//
//  AppStateTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 07/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

class AppStateTests: XCTestCase {
   
   var appState: AppState!
   
   override func setUpWithError() throws {
      appState = AppState()
   }
   
   override func tearDownWithError() throws {
      appState = nil
   }
   
   func testServiceStateUpdatesWithSuccess() throws {
      appState.serviceStarted()
      
      XCTAssertTrue(appState.serviceIsRunning, "`serviceIsRunning` should be true.")
      XCTAssertFalse(appState.serviceFailed, "`serviceFailed` should be false.")
      
      appState.serviceEnded(success: true)
      
      XCTAssertFalse(appState.serviceIsRunning, "`serviceIsRunning` should be false.")
      XCTAssertFalse(appState.serviceFailed, "`serviceFailed` should be false.")
   }
   
   func testServiceStateUpdatesWithFailure() throws {
      appState.serviceStarted()
      
      XCTAssertTrue(appState.serviceIsRunning, "`serviceIsRunning` should be true.")
      XCTAssertFalse(appState.serviceFailed, "`serviceFailed` should be false.")
      
      appState.serviceEnded(success: false)
      
      XCTAssertFalse(appState.serviceIsRunning, "`serviceIsRunning` should be false.")
      XCTAssertTrue(appState.serviceFailed, "`serviceFailed` should be true.")
   }
   
   func testDetailViewStateUpdates() throws {
      appState.detailViewAppeared()
      
      XCTAssertTrue(appState.detailIsVisible, "`detailIsVisible` should be true.")
      
      appState.detailViewDisappeared()
      
      XCTAssertFalse(appState.detailIsVisible, "`detailIsVisible` should be false.")
   }
   
}
