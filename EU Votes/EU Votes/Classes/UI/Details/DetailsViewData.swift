//
//  DetailsViewData.swift
//  EU Votes
//
//  Created by Quentin Biset on 16/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import Foundation
import DomainModel

struct DetailsViewData {
    // MARK: Constants
    private enum K {
        static let dateFormatter: DateFormatter = {
           let dateFormatter: DateFormatter = DateFormatter()
           dateFormatter.dateStyle = .full
           dateFormatter.timeStyle = .none
           return dateFormatter
        }()
    }

    // MARK: Properties
    let dateString: String
    let title: String
    let votes: [VoteCellViewData]

    // MARK: Initialisation
    init() {
        self.init(dateString: "", title: "", votes: [])
    }

    init(dateString: String, title: String, votes: [VoteCellViewData]) {
        self.dateString = dateString
        self.title = title
        self.votes = votes
    }

    init(proposal: any Proposal) {
        let dateString: String = K.dateFormatter.string(from: proposal.date)
        let votes: [VoteCellViewData] = proposal.votes.map { VoteCellViewData(vote: $0) }

        self.init(dateString: dateString, title: proposal.title, votes: votes)
    }
}
