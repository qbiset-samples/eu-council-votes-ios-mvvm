//
//  Proposal.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class represents one of the proposals the EU Council has voted on.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Proposal: Identifiable {
    // MARK: Properties
    /**
     An arbitrary identification number, received from the web API.
     */
    var id: Int { get }
    /**
     The full title of the proposal.
     */
    var title: String { get }
    /**
     The date the proposal was voted on.
     */
    var date: Date { get }
    /**
     An array of `Vote` objects, describing the various countries' vote.
     */
    var votes: [any Vote] { get }
}
