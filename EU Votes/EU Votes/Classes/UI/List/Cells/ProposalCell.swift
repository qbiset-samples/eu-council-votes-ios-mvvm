//
//  ProposalCell.swift
//  EU Votes
//
//  Created by Quentin Biset on 21/03/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A custom UITableViewCell that will be displayed in `ListViewController`.
 
 This cell displays the date and title of a EU council proposal (see `Proposal`).
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
class ProposalCell: UITableViewCell {
   // MARK: Properties
   /**
    Top label, used to display the date of the proposal.
    */
   @IBOutlet weak var dateLabel: UILabel!
   /**
    Bottom label, used to display the bottom of the proposal.
    */
   @IBOutlet weak var titleLabel: UILabel!
   
   // MARK: Display update
   /**
    Updates the cell's subviews with the relevant data from a `Proposal`, through a `ProposalCellData`.

    This function is thread-safe.
    
    - Parameter content: The content object that contains the `Proposal`'s formatted data.
    */
   func updateDisplay(withContent content: ProposalCellData) {
      DispatchQueue.main.async { [weak self] in
         self?.dateLabel.text = content.dateString
         self?.titleLabel.text = content.title
      }
   }

}
