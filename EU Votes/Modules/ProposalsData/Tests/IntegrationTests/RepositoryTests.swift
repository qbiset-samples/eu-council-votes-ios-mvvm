//
//  RepositoryTests.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 07/02/2024.
//

import Foundation

import XCTest
@testable import ProposalsDataRepositories
import DomainRepositories

final class RepositoryTests: XCTestCase {

    func testRepository() async throws {
        let repo = ProposalsRepository()

        // Testing fetch
        let result = await repo.fetchData()

        if case .failure(let error) = result {
            XCTFail(error.localizedDescription)
        }

        // Testing data was properly saved locally
        let expectation = XCTestExpectation(description: "Expecting values from proposals publisher.")

        let cancellable = repo.getProposals().sink(receiveCompletion: { completion in
            if case .failure(let error) = completion {
                XCTFail(error.localizedDescription)
            }
        }, receiveValue: { proposals in
            XCTAssertGreaterThan(proposals.count, 0)
            expectation.fulfill()
        })

        await fulfillment(of: [expectation], timeout: 20)
        cancellable.cancel()
    }

}
