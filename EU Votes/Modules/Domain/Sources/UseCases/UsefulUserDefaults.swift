//
//  UsefulUserDefaults.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 15/01/2024.
//

import Foundation

internal class UsefulUserDefaults {
    private struct K {
        static let fetchTimestampKey: String = "SuccessfulFetchTimestamp"
    }

    class func getLastSuccessfulFetchTimestamp() -> TimeInterval {
        return UserDefaults.standard.double(forKey: K.fetchTimestampKey)
    }

    class func storeSuccessfulFetchTimestamp() {
        UserDefaults.standard.set(Date.timeIntervalSinceReferenceDate, forKey: K.fetchTimestampKey)
    }
}
