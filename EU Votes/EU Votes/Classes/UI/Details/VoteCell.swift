//
//  VoteCell.swift
//  EU Votes
//
//  Created by Quentin Biset on 20/07/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A UITableViewCell that will be displayed in `DetailsViewController`.
 
 This cell displays the name of a country and its vote on a EU council proposal, using the system's "Right detail" style (no custom subviews).
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
class VoteCell: UITableViewCell {
   // MARK: Display update
   /**
    Updates the cell's subviews with the relevant data from a `Vote`, using a `VoteCellViewData`.

    This function is thread-safe.
    
    - Parameter content: The view model that prepared a `Vote`'s data for display.
    */
   func updateDisplay(withContent content: VoteCellViewData) {
      DispatchQueue.main.async { [weak self] in
         self?.textLabel?.text = content.countryName
         self?.detailTextLabel?.text = content.vote
      }
   }
   
}
