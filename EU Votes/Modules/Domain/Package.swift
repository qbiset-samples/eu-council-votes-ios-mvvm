// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Domain",
    platforms: [
        // The minimum versions are chosen for their support of the Identifiable protocol.
        .macOS(.v11),
        .macCatalyst(.v14),
        .iOS(.v14),
        .tvOS(.v14),
        .watchOS(.v7),
        .visionOS(.v1),
    ],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "Domain",
            targets: ["Model", "Repositories", "UseCases"]
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(name: "Model"),
        .target(
            name: "Repositories",
            dependencies: [.target(name: "Model")]
        ),
        .target(
            name: "UseCases",
            dependencies: [.target(name: "Model"), .target(name: "Repositories")],
            resources: [.process("Resources")]
        ),
        .testTarget(
            name: "DomainTests",
            dependencies: [.target(name: "Model"), .target(name: "Repositories"), .target(name: "UseCases")]
        ),
    ]
)
