//
//  ErrorCell.swift
//  EU Votes
//
//  Created by Quentin Biset on 17/07/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A UITableViewCell that will be displayed in `ListViewController` if a loading/error cell needs to be displayed.
 
 This cell shows a localized label using the system's basic style (no custom subviews).
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
class ErrorCell: UITableViewCell {
   
   // MARK: Display update
   /**
    Updates the text of the error cell.
    
    This function is thread-safe.
    
    - Parameter content: The content object from which to get the new text.
    */
   func updateDisplay(withContent content: SpecialCellData) {
      DispatchQueue.main.async { [weak self] in
         self?.textLabel?.text = content.text
      }
   }
   
}
