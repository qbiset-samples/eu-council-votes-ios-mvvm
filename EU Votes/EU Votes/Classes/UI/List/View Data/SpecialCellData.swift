//
//  SpecialCellData.swift
//  EU Votes
//
//  Created by Quentin Biset on 20/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import Foundation

// MARK: SpecialCellData
protocol SpecialCellData {
    var text: String { get }
}

// MARK: - LoadingCellData
struct LoadingCellData: SpecialCellData {
    // MARK: Constants
    private enum K {
        static let textLocalizationKey: String = "list.loadingCell.text"
        static let textLocalizationComment: String = "Title of the loading cell"
    }

   // MARK: Properties
   /** The title of a loading cell. */
   let text: String

   // MARK: Initialization
   init() {
       self.text = NSLocalizedString(K.textLocalizationKey, comment: K.textLocalizationComment)
   }
}

// MARK: - ErrorCellData
struct ErrorCellData: SpecialCellData {
    // MARK: Constants
    private enum K {
        static let firstRequestLocalizationKey: String = "list.errorCell.title.noData"
        static let firstRequestLocalizationComment: String = "Text shown by the error cell if the webservice request failed and no local data is available"

        static let refreshLocalizationKey: String = "list.errorCell.title.noRefresh"
        static let refreshLocalizationComment: String = "Text shown by the error cell if the webservice request failed but local data is still available"
    }

    // MARK: Properties
    /** The text of the error cell. */
    let text: String

    // MARK: Initialization
    init(areProposalsEmpty: Bool) {
        if areProposalsEmpty {
            self.text = NSLocalizedString(K.firstRequestLocalizationKey, comment: K.firstRequestLocalizationComment)
        } else {
            self.text = NSLocalizedString(K.refreshLocalizationKey, comment: K.refreshLocalizationComment)
        }
    }
}
