//
//  WebApiParsing.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 07/02/2024.
//

import Foundation
import OSLog

/**
 A utility class to parse the raw data received from the web API.

 The parsing is only a matter of using JSONDecoder; the main purpose of this class is easier unit testing.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class WebApiParsing {
    class func parseCountriesData(_ data: Data, logger: Logger? = nil) throws -> [WebApiCountry] {
        do {
            let decodedResponse: WebApiCountriesResponse = try JSONDecoder().decode(WebApiCountriesResponse.self, from: data)
            return decodedResponse.countries.sorted { $0.id < $1.id }
        } catch {
            logger?.error("Failed to parse countries JSON: \(error.localizedDescription)")
            throw WebApiErrorFactory.error(ofType: .jsonDecoding, withUnderlyingError: error)
        }
    }

    class func parseProposalsData(_ data: Data, logger: Logger? = nil) throws -> [RawProposal] {
        do {
            let decodedResponse: WebApiProposalsResponse = try JSONDecoder().decode(WebApiProposalsResponse.self, from: data)
            return decodedResponse.proposals
        } catch {
            logger?.error("Failed to parse proposals JSON: \(error.localizedDescription)")
            throw WebApiErrorFactory.error(ofType: .jsonDecoding, withUnderlyingError: error)
        }
    }
}
