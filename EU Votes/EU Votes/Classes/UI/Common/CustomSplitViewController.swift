//
//  CustomSplitViewController.swift
//  EU Votes
//
//  Created by Quentin Biset on 20/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A custom UISplitViewController that holds an instance of `ProposalsSplitViewModel` for convenience access to both its master and details children.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class CustomSplitViewController: UISplitViewController {
    var viewModel: ProposalsSplitViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let injector = (UIApplication.shared.delegate as! AppDelegate).injector
        self.viewModel = injector.getProposalsSplitViewModel()
    }
}
