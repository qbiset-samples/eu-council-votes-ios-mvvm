//
//  RawProposal.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 23/01/2024.
//

import Foundation

/**
 A `Decodable` object used in `ProposalsResponse` to parse the JSON data received from the API.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class RawProposal: Decodable {
    // MARK: Properties
    /**
     An arbitrary identification number.
     */
    let id: Int
    /**
     The title of the proposal, in English.
     */
    let title: String
    /**
     The date the EU Council voted on the proposal.
     */
    let date: Date
    /**
     The votes of all countries.
     */
    let votes: [RawVote]

    // MARK: Decodable
    private enum CodingKeys: String, CodingKey {
        case id = "vote_id"
        case title = "vote_title"
        case date = "date"
        case votes = "votes"
    }

    /**
     Create an instance of `RawProposal` by parsing JSON data.

     - SeeAlso: `RawVote` to see how the votes are parsed.
     */
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // The identifier is represented as a String in the JSON.
        let proposalId: String = try container.decode(String.self, forKey: .id)
        self.id = Int(proposalId) ?? 0

        // The string we receive in "vote_title" is usually unclean, so we have to trim white spaces and \r characters to make it a bit more readable.
        let tempTitle = try container.decode(String.self, forKey: .title)
        self.title = tempTitle.trimmingCharacters(in: .whitespacesAndNewlines)

        // The date is returned as a YYYY-MM-DD String in the JSON.
        let dateString: String = try container.decode(String.self, forKey: .date)
        let dateFormatter: ISO8601DateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        self.date = dateFormatter.date(from: dateString) ?? Date()

        let votes: [RawVote] = try container.decode([RawVote].self, forKey: .votes)
        self.votes = votes
    }
}
