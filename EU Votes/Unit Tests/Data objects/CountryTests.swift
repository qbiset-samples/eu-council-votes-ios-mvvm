//
//  CountryTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 07/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

class CountryTests: XCTestCase {
   
   func testEquatable() throws {
      let country: Country = MockCountries.france
      let countryCopy: Country = CountryTests.copyCountry(country)
      
      XCTAssertEqual(country, countryCopy, "`country` and `countryCopy` should be considered equal despite being two different instances.")
      
      XCTAssertNotEqual(country, MockCountries.germany, "`MockCountries.france` and `MockCountries.germany` should not be considered equal.")
      
      let countryCopyWithDummyName: Country = Country(identifier: country.id, name: "Dummy name", voteWeight: country.voteWeight)
      XCTAssertEqual(country, countryCopyWithDummyName, "`country` and `countryCopyWithDummyName` should be considered equal despite having different names.")
   }
   
   class func copyCountry(_ country: Country) -> Country {
      return Country(identifier: country.id, name: country.name, voteWeight: country.voteWeight)
   }
   
}
