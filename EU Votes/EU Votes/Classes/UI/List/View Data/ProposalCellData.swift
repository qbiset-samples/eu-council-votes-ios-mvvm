//
//  ProposalCellData.swift
//  EU Votes
//
//  Created by Quentin Biset on 17/07/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import Foundation
import DomainModel

/**
 This class will be the source of truth for a `ProposalCell`.

 Its main responsibilities are to format and expose the date and title of a `Proposal`.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class ProposalCellData {
    // MARK: Static properties
    /**
     The shared date formatter used to create `dateString` in instances of `ProposalCellData`.
     */
    static let dateFormatter: DateFormatter = {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()

    // MARK: Properties
    /** The proposal's identifier. */
    let identifier: Int
    /** The string representation of the proposal's date of vote. */
    let dateString: String
    /** The title of the proposal. */
    let title: String

    // MARK: Initialization
    init(proposal: any Proposal) {
        self.identifier = proposal.id
        self.dateString = ProposalCellData.dateFormatter.string(from: proposal.date)
        self.title = proposal.title
    }
}
