//
//  Country.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class is used to contain various data about the countries voting on the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Country: Identifiable {
    // MARK: Properties
    /**
     An arbitrary identification number, received from the web API.
     */
    var id: Int { get }
    /**
     The name of the country, in English.
     */
    var name: String { get }
    /**
     Weight of the country's vote.
     */
    var voteWeight: Int { get }
}
