//
//  ProposalsSplitViewUseCase.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 15/01/2024.
//

import Foundation
import OSLog
import Combine
import Model
import Repositories

/**
 Executes the business logic needed to display a list of proposals.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public class ProposalsSplitViewUseCase {
    // MARK: Enums
    private enum K {
        static let secondsInOneDay: Double = 86400
    }

    /**
     This will be used by the relevant view models to know if they should display a loading indicator or an error message.
     */
    public enum Status {
        case idle, fetching, error
    }

    // MARK: Private properties
    private let repository: ProposalsRepository
    private let logger = Logger(subsystem: "com.qbiset.EU-Votes.UseCases", category: "ProposalsSplitViewUseCase")
    private var _status = CurrentValueSubject<Status, Never>(.idle)

    // MARK: Initialisation
    public init(repository: ProposalsRepository) {
        self.repository = repository
    }

    // MARK: Common
    public func getStatusPublisher() -> AnyPublisher<Status, Never> {
        return _status.eraseToAnyPublisher()
    }
}

// MARK: - List View Controller
public extension ProposalsSplitViewUseCase {
    // MARK: Getters
    /**
     Returns the publisher broadcasting the list of proposals available locally.
     */
    func getProposalsPublisher() -> AnyPublisher<[any Proposal], Never> {
        return repository.getProposalsPublisher()
    }

    // MARK: Fetching data
    /**
     Fetches all necessary data from the web API if it has been at least a day since the last successful fetch (or if the data has never been fetched before), and stores it locally.
     */
    func loadDataIfNeeded() async {
        let lastFetchTimestamp = UsefulUserDefaults.getLastSuccessfulFetchTimestamp()
        guard (repository.isStorageEmpty ||
               lastFetchTimestamp == 0 ||
               Date.timeIntervalSinceReferenceDate > (lastFetchTimestamp + K.secondsInOneDay))
        else { return }

        await loadData()
    }

    /**
     Fetches all necessary data from the web API and stores it locally.
     */
    func loadData() async {
        guard _status.value != .fetching else { return }

        _status.value = .fetching
        let result = await repository.fetchData()
        switch result {
        case .success:
            UsefulUserDefaults.storeSuccessfulFetchTimestamp()
            _status.value = .idle
        case .failure(let error):
            logger.error("Failed to load data: \(error.localizedDescription)")
            _status.value = .error
            break
        }
    }
}

// MARK: - Details View Controller
public extension ProposalsSplitViewUseCase {
    /**
     Returns a publisher boardcasting the data of particular proposal (the data will thus be updated should it change).
     */
    func getProposalPublisher(withId id: Int) -> AnyPublisher<Result<any Proposal, ProposalsRepositoryError>, Never> {
        return repository.getProposalPublisher(withId: id)
    }
}
