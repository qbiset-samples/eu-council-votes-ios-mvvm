//
//  WebApiTests.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 07/02/2024.
//

import XCTest
import OSLog
@testable import WebApiDataSource

final class WebApiTests: XCTestCase {

    private let logger = Logger(subsystem: "com.qbiset.EU-Votes.ProposalsData", category: "WebApiTests")

    // Testing that the sample JSON in the resources is parsed correctly. This should yield 27 Country objects.
    func testCountriesJsonParsing() throws {
        do {
            let jsonURL: URL = try XCTUnwrap(Bundle.module.url(forResource: "test_country_data", withExtension: "json"))
            let rawData: Data = try Data(contentsOf: jsonURL)

            let countries: [WebApiCountry] = try WebApiParsing.parseCountriesData(rawData, logger: logger)

            XCTAssertEqual(countries.count, 27, "The parser should return a set of 27 Country objects.")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    // Testing that the sample JSON in the resources is parsed correctly. This should yield 5 RawProposal objects.
    func testProposalsJsonParsing() throws {
        do {
            let jsonURL: URL = try XCTUnwrap(Bundle.module.url(forResource: "test_proposal_data", withExtension: "json"))
            let rawData: Data = try Data(contentsOf: jsonURL)

            let proposals: [RawProposal] = try WebApiParsing.parseProposalsData(rawData, logger: logger)

            XCTAssertEqual(proposals.count, 5, "The parser should return an array of 5 RawProposal objects.")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
}
