//
//  WebApiCountriesResponse.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 24/01/2024.
//

import Foundation

/**
 This `Decodable` object is used to parse the countries' JSON data retrieved in `WebApiDataSource`.

 The data we receive from the API is not an array of countries as we might expect, but a dictionary with arbitrary keys. As such we have to get the keys manually and loop through the dictionary to parse each country as a `WebApiCountry`.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct WebApiCountriesResponse: Decodable {
    // MARK: Properties
    /**
     A set of `WebApiCountry` objects, parsed from JSON.
     */
    let countries: Set<WebApiCountry>

    // MARK: Decodable
    private struct DynamicKey: CodingKey {
        var intValue: Int?
        var stringValue: String

        init?(intValue: Int) {
            self.intValue = intValue
            self.stringValue = "\(intValue)"
        }

        init?(stringValue: String) {
            self.stringValue = stringValue
        }
    }

    /**
     Creates an instance of `WebApiCountriesResponse` by parsing JSON data.

     The original dictionary is iterated through, and each value is parsed as a `WebApiCountry` and added to the `countries` property.
     */
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DynamicKey.self)

        var countries: Set<WebApiCountry> = Set<WebApiCountry>()

        for key: DynamicKey in container.allKeys {
            let country: WebApiCountry = try container.decode(WebApiCountry.self, forKey: key)
            countries.insert(country)
        }

        self.countries = countries
    }
}
