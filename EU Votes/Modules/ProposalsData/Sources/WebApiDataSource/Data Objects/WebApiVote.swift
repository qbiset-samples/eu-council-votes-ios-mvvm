//
//  WebApiVote.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 23/01/2024.
//

import Foundation
import DomainModel

/**
 After a `RawVote` was decoded, it should be transformed into this structure using the correct `Country` object before being returned by the Data Source.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct WebApiVote: Vote {
    // MARK: Properties
    /**
     The country that casted the vote.
     */
    let country: any Country
    /**
     The value of the vote.

     - Note: I cannot find a definitive list of possible values here, or we could have used an enum for this property.
     */
    let value: String

    // MARK: Initialisation
    static func build(fromRawVote rawVote: RawVote, withPossibleCountries countries: any Collection<Country>) throws -> WebApiVote {
        let country: any Country = countries.first { $0.id == rawVote.countryId }!
        return WebApiVote(country: country, value: rawVote.value)
    }
}
