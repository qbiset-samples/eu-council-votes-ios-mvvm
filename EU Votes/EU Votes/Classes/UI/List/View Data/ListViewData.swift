//
//  ListViewData.swift
//  EU Votes
//
//  Created by Quentin Biset on 12/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import Foundation

struct ListViewData {
    // MARK: Properties
    let shouldEnableRefresh: Bool
    let specialSection: [any SpecialCellData]
    let proposalsSection: [ProposalCellData]

    // MARK: Initialisation
    init() {
        self.init(shouldEnableRefresh: false, specialSection: [], proposalsSection: [])
    }

    init(shouldEnableRefresh: Bool, specialSection: [any SpecialCellData], proposalsSection: [ProposalCellData]) {
        self.shouldEnableRefresh = shouldEnableRefresh
        self.specialSection = specialSection
        self.proposalsSection = proposalsSection
    }
}
