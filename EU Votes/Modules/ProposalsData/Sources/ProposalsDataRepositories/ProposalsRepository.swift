//
//  ProposalsRepository.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 18/01/2024.
//

import Foundation
import Combine
import DomainModel
import DomainRepositories
import StorageDataSource
import WebApiDataSource

public class ProposalsRepository : DomainRepositories.ProposalsRepository {
    // MARK: Private properties
    private let localDataSource: StorageDataSource = StorageDataSource()
    private let remoteDataSource: WebApiDataSource = WebApiDataSource()

    // MARK: Initialisation
    public init() {}

    // MARK: Computed properties
    public var isStorageEmpty: Bool { localDataSource.isEmpty }

    // MARK: Functions
    public func fetchData() async -> Result<Void, ProposalsRepositoryError> {
        do {
            let countries = try await remoteDataSource.getCountries()
            localDataSource.saveCountries(countries)

            let proposals = try await remoteDataSource.getProposals(withCountries: countries)
            localDataSource.saveProposals(proposals)

            return .success(())
        } catch {
            return .failure(ProposalsRepositoryError.fetchError)
        }
    }

    /**
     Retrieves a Publisher of all Proposal items in storage.
     */
    public func getProposalsPublisher() -> AnyPublisher<[any Proposal], Never> {
        return localDataSource.proposals.eraseToAnyPublisher()
    }

    /**
     Retrieves a Publisher for a single Proposal item found in storage.

     Can throw `ProposalsRepositoryError.proposalNotFound`, although it will theoretically never happen: in order to open the details view controller which uses this publisher, the user would have needed to select the proposal in the list.
     */
    public func getProposalPublisher(withId id: Int) -> AnyPublisher<Result<any Proposal, ProposalsRepositoryError>, Never> {
        return localDataSource.proposals.map { proposals in
            let result: Result<any Proposal, ProposalsRepositoryError>

            if let proposal: any Proposal = proposals.first(where: { $0.id == id }) {
                result = Result.success(proposal)
            } else {
                result = Result.failure(ProposalsRepositoryError.proposalNotFound)
            }

            return result
        }.eraseToAnyPublisher()
    }
}
