//
//  DetailsStatusView.swift
//  EU Votes
//
//  Created by Quentin Biset on 11/05/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A view used with `DetailsViewController` to show a variety of status updates.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class DetailsStatusView: UIView {
    // MARK: Constants
    private enum K {
        static let defaultAnimationDuration: TimeInterval = 0.5
        static let fullAlpha: CGFloat = 1.0
        static let zeroAlpha: CGFloat = CGFloat.zero
    }

    // MARK: Properties
    /** Main label. */
    @IBOutlet weak var titleLabel: UILabel!
    /** Activity indicator view, placed below the label. */
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Display update
    /** Updates the UI according to the view data. */
    func updateSubviews(withViewData viewData: DetailsStatusViewData) {
        titleLabel.text = viewData.title
        if viewData.shouldShowActivityIndicator {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

    // MARK: Transitions
    /**
     Shows the loading view.

     This method is thread-safe since we take care of running the transition on the main thread.

     - Parameter animated: Whether to animate the transition or not.
     */
    func show(animated: Bool) {
        if self.isHidden {
            DispatchQueue.main.async { [weak self] in
                self?.alpha = K.zeroAlpha
                self?.isHidden = false
                self?.transitionToOpaque(animated: animated)
            }
        }
    }

    /**
     Starts a fade-in animation.

     This function is **not** thread-safe: it falls to you to ensure it will run on the main thread.

     - Parameter animated: Whether to animate the transition or not.
     */
    private func transitionToOpaque(animated: Bool) {
        let animationBlock: () -> Void = { [weak self] in
            self?.alpha = K.fullAlpha
        }

        if animated {
            UIView.animate(withDuration: K.defaultAnimationDuration, animations: animationBlock)
        } else {
            animationBlock()
        }
    }

    /**
     Hides the loading view.

     This method is thread-safe since we take care of running the transition on the main thread.

     - Parameter animated: Whether to animate the transition or not.
     */
    func hide(animated: Bool) {
        if !self.isHidden {
            let animationBlock: () -> Void = { [weak self] in
                self?.alpha = K.zeroAlpha
            }

            let completionBlock: (Bool) -> Void = { [weak self] (finished: Bool) in
                self?.activityIndicator.stopAnimating()
                self?.isHidden = true
            }

            DispatchQueue.main.async {
                if animated {
                    UIView.animate(withDuration: K.defaultAnimationDuration,
                                   animations: animationBlock,
                                   completion: completionBlock)
                } else {
                    animationBlock()
                    completionBlock(true)
                }
            }
        }
    }

}
