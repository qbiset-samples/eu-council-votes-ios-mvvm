//
//  WebApiCountry.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 18/01/2024.
//

import Foundation
import DomainModel

/**
 A `Decodable` object used in `WebApiDataSource` to parse the JSON country data received from the API.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct WebApiCountry: Country, Decodable, Hashable {
    // MARK: Properties
    /**
     An arbitrary identification number.
     */
    let id: Int
    /**
     The name of the country, in English.
     */
    let name: String
    /**
     The weight of the country's vote.
     */
    let voteWeight: Int

    // MARK: Initialisation
    init(id: Int, name: String, voteWeight: Int) {
        self.id = id
        self.name = name
        self.voteWeight = voteWeight
    }

    // MARK: Decodable
    private enum CodingKeys: String, CodingKey {
        case id = "country_id"
        case name = "country"
        case voteWeight = "vote_weight"
    }

    init(from decoder: Decoder) throws {
        do {
            let container: KeyedDecodingContainer = try decoder.container(keyedBy: CodingKeys.self)

            // The identifier is represented as a String in the JSON.
            let id: String = try container.decode(String.self, forKey: .id)
            self.id = Int(id) ?? 0

            self.name = try container.decode(String.self, forKey: .name)

            // The vote weight is represented as a String in the JSON.
            let voteWeight: String = try container.decode(String.self, forKey: .voteWeight)
            self.voteWeight = Int(voteWeight) ?? 0
        }
        catch {
            throw error
        }
    }
}
