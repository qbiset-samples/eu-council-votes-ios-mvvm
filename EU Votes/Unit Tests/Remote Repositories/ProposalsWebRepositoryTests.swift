//
//  ProposalsWebRepositoryTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 05/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

// This class will mostly test the ProposalsDataParser class used by ProposalsWebRepository.
// The rest of the class is tied to a web API and is more suited for an integration test.
class ProposalsWebRepositoryTests: XCTestCase {
   
   // Testing that a data task returning with an error will cause the parser to throw a ServiceError with the correct domain and code.
   func testParsingWithWebError() throws {
      let parser: ProposalsDataParser = ProposalsDataParser()
      let dummyError: Error = NSError(domain: "ProposalsWebRepositoryTests", code: 0, userInfo: nil)
      
      XCTAssertThrowsError(try parser.parseResponse(data: nil, response: nil, error: dummyError),
                           "This should have thrown a ServiceError") { error in
         XCTAssertEqual((error as NSError).domain, ProposalsServiceError.RepositoryIdentifier.proposals.rawValue)
         XCTAssertEqual((error as NSError).code, ProposalsServiceError.ErrorType.urlSession.rawValue)
      }
   }
   
   // Testing that a data task returning no data despite no error being raised will cause the parse to throw a ServiceError with the correct domain and code.
   func testParsingWithNoData() throws {
      let parser: ProposalsDataParser = ProposalsDataParser()
      
      XCTAssertThrowsError(try parser.parseResponse(data: nil, response: nil, error: nil), "This should have thrown a ServiceError") { error in
         XCTAssertEqual((error as NSError).domain, ProposalsServiceError.RepositoryIdentifier.proposals.rawValue)
         XCTAssertEqual((error as NSError).code, ProposalsServiceError.ErrorType.noData.rawValue)
      }
   }
   
   // We keep this part in its own function because we will be using it in another test class.
   class func parseTestData() throws -> [RawProposal] {
      let jsonURL: URL = Bundle(for: ProposalsWebRepositoryTests.classForCoder()).url(forResource: "test_proposal_data", withExtension: "json")!
      let rawData: Data = try Data(contentsOf: jsonURL)
      
      let parser: ProposalsDataParser = ProposalsDataParser()
      return try parser.parseResponse(data: rawData, response: nil, error: nil)
   }
   
   // Testing that the sample JSON in the resources is parsed correctly. This should yield 27 Country objects.
   func testParserWithMockData() throws {
      let rawProposals: [RawProposal] = try ProposalsWebRepositoryTests.parseTestData()
      
      XCTAssertEqual(rawProposals.count, 5, "The parser should return an array of 5 RawProposal objects.")
   }
   
}
