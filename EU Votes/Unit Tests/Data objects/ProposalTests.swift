//
//  ProposalTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 07/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

class ProposalTests: XCTestCase {
   
   func testEquatable() throws {
      let proposal: Proposal = MockProposals.proposal1
      let proposalCopy: Proposal = ProposalTests.copyProposal(proposal)
      
      XCTAssertEqual(proposal, proposalCopy, "`proposal` and `proposalCopy` should be considered equal despite being two different instances.")
      
      XCTAssertNotEqual(proposal, MockProposals.proposal2, "`MockProposals.proposal1` and `MockProposals.proposal2` should not be considered equal.")
      
      let proposalCopyWithDummyName: Proposal = Proposal(identifier: proposal.id, title: "Dummy name", date: proposal.date, votes: proposal.votes)
      
      XCTAssertEqual(proposal, proposalCopyWithDummyName, "`proposal` and `proposalCopyWithDummyName` should be considered equal despite having different names.")
   }
   
   class func copyProposal(_ proposal: Proposal) -> Proposal {
      return Proposal(identifier: proposal.id, title: proposal.title, date: proposal.date, votes: proposal.votes)
   }
   
}
