//
//  LocalRepositoryTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 07/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

class LocalRepositoryTests: XCTestCase {
   
   func testQueriesAndUpdates() throws {
      let repository: LocalRepository = LocalRepository()
      
      XCTAssertTrue(repository.isEmpty, "Repository should be empty.")
      
      repository.updateProposals(MockProposals.all)
      
      XCTAssertFalse(repository.isEmpty, "Repository should no longer be empty.")
      
      let proposal: Proposal = ProposalTests.copyProposal(MockProposals.proposal2)
      let foundProposal: Proposal? = repository.findProposalEqualTo(proposal)
      
      XCTAssertEqual(foundProposal, MockProposals.proposal2, "Repository should have returned the same instance as MockProposals.proposal2")
   }
   
}
