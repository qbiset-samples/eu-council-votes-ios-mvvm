//
//  Vote.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class represents a country's vote on a proposal.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Vote {
    // MARK: Properties
    /**
     The country casting the vote.
     */
    var country: any Country { get }
    /**
     The value of the vote.

     - Note: I cannot find a definitive list of possible values here, or we could have used an enum for this property.
     */
    var value: String { get }
}
