//
//  LoadingCell.swift
//  EU Votes
//
//  Created by Quentin Biset on 04/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit

/**
 A custom UITableViewCell that will be displayed in `ListViewController` when the view model is trying  to fetch up-to-date data from the web API.

 This cell shows a localized label and an activity indicator.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class LoadingCell: UITableViewCell {
    // MARK: Properties
    /**
     A localized label.

     Its content is set in `awakeFromNib()`.
     */
    @IBOutlet weak var titleLabel: UILabel!
    /**
     A activity indicator.

     The animation will need to be restarted manually in UITableViewDelegate's [tableView(_:willDisplay:forRowAt:)](https://developer.apple.com/documentation/uikit/uitableviewdelegate/1614883-tableview) method.
     */
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    // MARK: Display update
    /**
     Updates the title of the loading cell.

     This function is thread-safe.

     - Parameter content: The content object from which to get the new title.
     */
    func updateDisplay(withContent content: SpecialCellData) {
        DispatchQueue.main.async { [weak self] in
            self?.titleLabel.text = content.text
        }
    }

}
