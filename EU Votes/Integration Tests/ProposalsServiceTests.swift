//
//  ProposalsServiceTests.swift
//  Integration Tests
//
//  Created by Quentin Biset on 05/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

/*
 Testing that main service does its job correctly, meaning:
 - fetching country data from web API and parsing it into Country objects,
 - fetching proposals data from web API and parsing it into RawProposal objects,
 - merging both sets of data into an array of Proposal objects and sending it to the completion handler,
 - sending local notifications to warn interested classes that the request is finished.
 */
class ProposalsServiceTests: XCTestCase {

   var proposalsService: ProposalsService?
   var promise: XCTestExpectation?
   private var observers: [NSObjectProtocol] = []

    override func setUpWithError() throws {
      proposalsService = ProposalsService()
      promise = expectation(description: "Success or failure notified.")
      self.addObservers()
    }

    override func tearDownWithError() throws {
      proposalsService = nil
      promise = nil
      self.removeObservers()
    }

   /*
    The completion handlers will be executed before the notifications are launched and fulfill the promise, so there shouldn't be any false negatives where `proposals` is empty because of asynchronicity.
    */
    func testService() throws {
      var proposals: [Proposal] = []
      
      let completionHandler: ProposalsService.CompletionHandler = { (parsedProposals: [Proposal]) in
         proposals.append(contentsOf: parsedProposals)
      }
      let errorHandler: ProposalsService.ErrorHandler = { (error: Error) in
         XCTFail(error.localizedDescription)
      }
      
      proposalsService!.startDataRetrieval(withCompletionHandler: completionHandler, andErrorHandler: errorHandler)
      wait(for: [promise!], timeout: 90)
      
      XCTAssertFalse(proposals.isEmpty, "The returned array of proposals should not be empty.")
    }
   
   /*
    Important: make sure that the observers are listening to the local proposalsService property.
    Depending on what you run the test on, it's possible the app may be running normally in parallel, in which case the singleton ProposalsService.default may also be fetching data at the same time.
    */
   private func addObservers() {
      let successBlock: (Notification) -> Void = { [weak self] (notification: Notification) in
         self?.promise?.fulfill()
      }
      
      let failureBlock: (Notification) -> Void = { [weak self] (notification: Notification) in
         self?.promise?.fulfill()
      }
      
      let successObserver: NSObjectProtocol = NotificationCenter.default.addObserver(forName: ProposalsService.NotificationNames.success, object: proposalsService, queue: nil, using: successBlock)
      self.observers.append(successObserver)
      let failureObserver: NSObjectProtocol = NotificationCenter.default.addObserver(forName: ProposalsService.NotificationNames.failure, object: proposalsService, queue: nil, using: failureBlock)
      self.observers.append(failureObserver)
   }
   
   private func removeObservers() {
      for observer: NSObjectProtocol in self.observers {
         NotificationCenter.default.removeObserver(observer)
      }
      
      self.observers.removeAll()
   }

}
