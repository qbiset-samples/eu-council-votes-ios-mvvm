//
//  DetailsViewController.swift
//  EU Votes
//
//  Created by Quentin Biset on 28/02/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit
import OSLog
import Combine

/**
 Our detail view controller, part of a [UISplitViewController](https://developer.apple.com/documentation/uikit/uisplitviewcontroller).
 
 It will display the date and full title of a EU council proposal, along with the voting records of all participating countries.
 
 If the device is an iPad, this view controller will be the app's point of entry. As a result, this view controller will use a loading view as a placeholder until the data has been retrieved. Once that is done, the loading view fades and the first proposal in the model is automatically selected. If the request failed, an error message is displayed instead.
 
 Note that once we have retrieved the data, if the user triggers `MasterViewController`'s refresh mechanism, the loading view is no longer used here and the content will be silently updated when the service is finished. Instead, `MasterViewController` (if it is visible) will be showing its loading cell. If the request fails, **DetailsViewController** will keep displaying the local data while `MasterViewController` will display its error cell.
 
 Most of this logic is delegated to `DetailsViewModel`, and **DetailsViewController** just needs to read `DetailViewModel`'s computed properties to display things accordingly.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class DetailsViewController: UIViewController {
// MARK: Constants
    private enum K {
        static let customSeparatorHeight: CGFloat = 0.5

        static let voteCellIdentifier: String = "VoteCell"
    }

   // MARK: Private properties
    /** Logger. */
    private let logger = Logger(subsystem: "com.qbiset.EU-Votes", category: "DetailsViewController")
    /** Set of cancellables, to listen to publishers from the view model. */
    private var cancellables = Set<AnyCancellable>()
    /** The view data needed to populate the details' views. */
    private var viewData = DetailsViewData()

    // MARK: IBOutlet properties
   /**
    A full loading view displaying a message to the user while the data is being retrieved.
    
    This should only appear on iPad where the app starts on **DetailsViewController**; on iPhone, the app starts on **MasterViewController** instead and there is no way to access **DetailsViewController** before the data has been loaded.
    */
   @IBOutlet weak var statusView: DetailsStatusView!
   /** The top label, displaying the date of the proposal. */
   @IBOutlet weak var dateLabel: UILabel!
   /** The label below the date, displaying the full title of the proposal. */
   @IBOutlet weak var titleLabel: UILabel!
   /** The table view at the bottom, showing the voting records. */
   @IBOutlet weak var votesTableView: UITableView!
   /** Layout Constraint handling the table view's height. */
   @IBOutlet weak var votesTableViewHeightConstraint: NSLayoutConstraint!
   /** Layout Constraint handling the custom separator at the top of the table view. */
   @IBOutlet weak var customSeparatorHeightConstraint: NSLayoutConstraint!

    // MARK: Computed properties
    /** The view model shared by both panes of the split view controller. */
    private var viewModel: ProposalsSplitViewModel? {
        return (splitViewController as? CustomSplitViewController)?.viewModel
    }

   // MARK: UIViewController methods
   override func viewDidLoad() {
      super.viewDidLoad()
       // We don't want to display an empty table view while the content is being processed, so we hide it with an height constraint of 0.0.
       self.votesTableViewHeightConstraint.constant = CGFloat.zero
       self.customSeparatorHeightConstraint.constant = K.customSeparatorHeight
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)

       viewModel?.areDetailsShowing = true

       viewModel?.detailsPublisher.receive(on: DispatchQueue.main)
           .sink(receiveValue: { [weak self] state in
               self?.handleState(state)
           }).store(in: &cancellables)
   }
   
   override func viewWillDisappear(_ animated: Bool) {
       viewModel?.areDetailsShowing = false
       cancellables.forEach { $0.cancel() }
      super.viewWillDisappear(animated)
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      // At this point the content size of the table view has been computed: we update the contraint if need be.
      if self.votesTableViewHeightConstraint.constant != self.votesTableView.contentSize.height {
         self.votesTableViewHeightConstraint.constant = self.votesTableView.contentSize.height
         self.view.setNeedsLayout()
      }
   }
   
}

// MARK: - Display update
extension DetailsViewController {
    /** Updates the UI depending on `state`. */
    private func handleState(_ state: ProposalsSplitViewModel.DetailsState) {
        switch state {
        case .status(let statusViewData):
            statusView.updateSubviews(withViewData: statusViewData)
            statusView.show(animated: true)
        case .normal(let detailsViewData):
            self.viewData = detailsViewData
            updateDetailsViews(viewData: detailsViewData)
            statusView.hide(animated: true)
        }
    }

    private func updateDetailsViews(viewData: DetailsViewData) {
        dateLabel.text = viewData.dateString
        titleLabel.text = viewData.title
        votesTableView.reloadData()
    }
}

// MARK: - UITableViewDataSource methods
extension DetailsViewController: UITableViewDataSource {
    // Number of rows ------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewData.votes.count
    }

    // Cells ------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewData: VoteCellViewData = viewData.votes[indexPath.row]
        let cell: VoteCell = tableView.dequeueReusableCell(withIdentifier: K.voteCellIdentifier, for: indexPath) as! VoteCell
        cell.updateDisplay(withContent: viewData)

        return cell
    }
}
