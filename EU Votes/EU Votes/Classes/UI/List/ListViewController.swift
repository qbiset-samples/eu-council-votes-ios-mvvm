//
//  ListViewController.swift
//  EU Votes
//
//  Created by Quentin Biset on 28/02/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import UIKit
import Combine
import OSLog

/**
 A UITableViewController that will serve as the master pane of our `CustomSplitViewController`.

 It will display a list of EU council proposals by ascending chronological order.

 If both panes are visible at app start, the details pane will be displaying a loading or error indicator when the view model tries to fetch the necessary data from the web API. If only this pane is visible, or if the detail pane is already showing a proposal's data, then this view controller will be charge of displaying the loading/error indicators.

 The underlying view model is stored in the CustomSplitViewController; this view controller should only need to subscribe to its **listPublisher** property to receive the view data needed to updates its views.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class ListViewController: UITableViewController {
    // MARK: Constants
    private enum K {
        static let detailsSegueIdentifier: String = "showDetails"

        static let numberOfSections: Int = 2

        static let loadingCellHeight: CGFloat = 54.0
        static let errorCellHeight: CGFloat = 87.0
        static let proposalCellHeight: CGFloat = 125.0

        static let loadingCellIdentifier: String = "LoadingCell"
        static let errorCellIdentifier: String = "ErrorCell"
        static let proposalCellIdentifier: String = "ProposalCell"
    }

    private enum SectionIndex: Int {
        case specialCells = 0
        case proposals = 1
    }

    // MARK: Private properties
    /** Logger. */
    private let logger = Logger(subsystem: "com.qbiset.EU-Votes", category: "ListViewController")
    /** Set of cancellables, to listen to publishers from the view model. */
    private var cancellables = Set<AnyCancellable>()
    /** The view data needed to populate the table view. */
    private var viewData = ListViewData()

    // MARK: IBOutlet properties
    /**
     A refresh button on the right side of the navigation bar.

     The model has no cancellation mechanism when fetching data, so we disable the button while the request is going.
     */
    @IBOutlet weak var refreshButton: UIBarButtonItem!

    // MARK: Computed properties
    /** The view model shared by both panes of the split view controller. */
    private var viewModel: ProposalsSplitViewModel? {
        return (splitViewController as? CustomSplitViewController)?.viewModel
    }

    // MARK: UIViewController methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed

        guard let viewModel = self.viewModel else {
            logger.warning("The shared view model was not found when viewWillAppear(_:) was called.")
            return
        }

        viewModel.listPublisher.receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] viewData in
                self?.viewData = viewData
                self?.updateDisplay()
            }).store(in: &cancellables)

        viewModel.fetchDataIfNeeded()
    }

    override func viewWillDisappear(_ animated: Bool) {
        cancellables.forEach { $0.cancel() }
        super.viewWillDisappear(animated)
    }

    // MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case K.detailsSegueIdentifier:
            prepareDetailViewController(usingSegue: segue)
        default:
            logger.error("prepare(for:sender:) called with unknown segue \"\(segue.identifier ?? "")\".")
        }
    }

    private func prepareDetailViewController(usingSegue segue: UIStoryboardSegue) {
        // No guards here; if we have an unwrapping problem here, the underlying issue is critical enough that we prefer to crash to make the problem obvious.
        let navController: UINavigationController = segue.destination as! UINavigationController
        let destination: DetailsViewController = navController.topViewController as! DetailsViewController
        destination.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        destination.navigationItem.leftItemsSupplementBackButton = true

        guard let indexPath: IndexPath = tableView.indexPathForSelectedRow,
              indexPath.row < viewData.proposalsSection.count
        else {
            logger.error("Tried to retrieve ID of proposal selected by the user, but the index path was undefined or out of bounds.")
            return
        }
        
        let proposalId: Int = viewData.proposalsSection[indexPath.row].identifier
        viewModel?.selectedProposalId = proposalId
    }

}

// MARK: - Display update
extension ListViewController {

    func updateDisplay() {
        tableView.reloadData()
        refreshButton.isEnabled = viewData.shouldEnableRefresh
    }
}

// MARK: - Table View
extension ListViewController {
    // Number of sections ------
    override func numberOfSections(in tableView: UITableView) -> Int {
        return K.numberOfSections
    }

    // Number of rows ------
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case SectionIndex.specialCells.rawValue:
            return viewData.specialSection.count
        case SectionIndex.proposals.rawValue:
            return viewData.proposalsSection.count
        default:
            return Int.zero
        }
    }

    // Height of rows ------
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        // Section 0 = Special cell; Section 1 = Proposals
        switch indexPath.section {
        case SectionIndex.specialCells.rawValue:
            return getSpecialCellHeight(indexPath: indexPath)
        case SectionIndex.proposals.rawValue:
            return K.proposalCellHeight
        default:
            return CGFloat.zero
        }
    }

    private func getSpecialCellHeight(indexPath: IndexPath) -> CGFloat {
        let cellData: any SpecialCellData = viewData.specialSection[indexPath.row]
        switch cellData {
        case is LoadingCellData:
            return K.loadingCellHeight
        case is ErrorCellData:
            return K.errorCellHeight
        default:
            return CGFloat.zero
        }
    }

    // Cells ------
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case SectionIndex.specialCells.rawValue:
            return dequeueAndPrepareSpecialCell(tableView: tableView, forIndexPath: indexPath)
        case SectionIndex.proposals.rawValue:
            return dequeueAndPrepareProposalCell(tableView: tableView, forIndexPath: indexPath)
        default:
            return createEmptyCell()
        }
    }

    private func dequeueAndPrepareSpecialCell(tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellData: any SpecialCellData = viewData.specialSection[indexPath.row]
        switch cellData {
        case is LoadingCellData:
            let cell = tableView.dequeueReusableCell(withIdentifier: K.loadingCellIdentifier, for: indexPath) as! LoadingCell
            cell.updateDisplay(withContent: cellData)
            return cell
        case is ErrorCellData:
            let cell = tableView.dequeueReusableCell(withIdentifier: K.errorCellIdentifier, for: indexPath) as! ErrorCell
            cell.updateDisplay(withContent: cellData)
            return cell
        default:
            return createEmptyCell()
        }
    }

    private func dequeueAndPrepareProposalCell(tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.proposalCellIdentifier, for: indexPath) as! ProposalCell

        let cellData: ProposalCellData = viewData.proposalsSection[indexPath.row]
        cell.updateDisplay(withContent: cellData)

        return cell
    }

    private func createEmptyCell() -> UITableViewCell {
        logger.warning("An empty cell was created, which should never happen.")
        return UITableViewCell(style: .default, reuseIdentifier: nil)
    }

    // Without this, the activity indicator view in the loading cell is stopped when the cell is reused.
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == SectionIndex.specialCells.rawValue,
           let cell: LoadingCell = cell as? LoadingCell {
            cell.activityIndicatorView.startAnimating()
        }
    }

    // No need to implement didSelect:, the storyboard already registers a segue on the relevant cells.
}

// MARK: - User interaction
extension ListViewController {
    /**
     Called when the refresh button in the navigation bar is tapped.

     We ask the view model to pull updated data from the web API; if another fetch attempt is already underway, nothing should happen (the button should have been disabled, and in any  case the UseCase will ignore any fetch request while one is already running). When new data is available, the relevant publisher will send updated view data and the views should be updated accordingly.

     - Parameter sender: The button that was tapped.
     */
    @IBAction func refreshButtonTapped(_ sender: UIBarButtonItem) {
        viewModel?.refreshDataModel()
    }
}
