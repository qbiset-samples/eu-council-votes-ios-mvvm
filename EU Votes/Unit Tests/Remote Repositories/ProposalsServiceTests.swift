//
//  ProposalsServiceTests.swift
//  Unit Tests
//
//  Created by Quentin Biset on 05/02/2021.
//  Copyright © 2021 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

// This class will mostly test the CountriesAndProposalsMerger class used by ProposalsService.
// The rest of the class is tied to a web API and is more suited for an integration test.
class ProposalsServiceTests: XCTestCase {
   
   func testMerger() throws {
      let countries: Set<Country> = try CountriesWebRepositoryTests.parseTestData()
      let rawProposals: [RawProposal] = try ProposalsWebRepositoryTests.parseTestData()
      
      let merger: CountriesAndProposalsMerger = CountriesAndProposalsMerger()
      let proposals: [Proposal] = merger.mergeIntoProposals(rawProposals: rawProposals, countries: countries)
      
      XCTAssertEqual(proposals.count, 5, "There should be 5 proposals after parsing and merger.")
      for proposal in proposals {
         XCTAssertGreaterThan(proposal.votes.count, 0, "The proposal \(proposal.id) has no votes, which shouldn't happen.")
      }
   }
   
}
